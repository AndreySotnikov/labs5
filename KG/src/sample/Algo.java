package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by andrey on 08.10.14.
 */
public class Algo {
    private int x_min;
    private int y_min;
    private int x_max;
    private int y_max;

    private int ln_x0;
    private int ln_y0;
    private int ln_x1;
    private int ln_y1;

    public int res_x0;
    public int res_x1;
    public int res_y0;
    public int res_y1;

    boolean find1 = false;

    GraphicsContext graphics;
    Drawing draw;
    PixelWriter writer;
    WritableImage img;
    Algo(GraphicsContext graphics, WritableImage img, int x_min, int y_min,int x_max,int y_max,int x0,int y0,int x1, int y1){
        this.graphics = graphics;
        this.img = img;
        this.writer = img.getPixelWriter();
        draw = new Drawing(graphics,writer);
        this.x_min=x_min;
        this.y_min=y_min;
        this.x_max=x_max;
        this.y_max=y_max;
        this.ln_x0=x0;
        this.ln_x1=x1;
        this.ln_y0=y0;
        this.ln_y1=y1;

        res_x0 = (x_max+x_min)/2;
        res_x1 = (x_max+x_min)/2;
        res_y0 = (y_max+y_min)/2;
        res_y1 = (y_max+y_min)/2;
    }

    private int getCode(int x, int y){
        return ((x<x_min)?1:0)+ //+1 если точка левее прямоугольника
                ((x>x_max)?2:0)+ //+2 если точка правее прямоугольника
                ((y<y_min)?4:0)+ //+4 если точка ниже прямоугольника
                ((y>y_max)?8:0); //+8 если точка выше прямоугольника
    }

    private int lineOrientation(int x1,int y1,int x2,int y2){
        int val1 = getCode(x1,y1);
        int val2 = getCode(x2,y2);
        if (val1==0 && val2==0)
            return 0;
        else
            if ((val1 & val2) !=0)
                return 1;
        return -1;
    }


    public int cut(){
        return Clipping(ln_x0,ln_y0,ln_x1,ln_y1);
    }

    public int Clipping(int x0,int y0,int x1,int y1){
        if (Math.pow((x1-x0),2)+Math.pow((y1-y0),2)<=2)
            return -1;
        if (lineOrientation(x0,y0,x1,y1)==1)
            return -1;
        if (lineOrientation(x0,y0,x1,y1)==0){
            if (!find1){
                res_y0=y0;
                res_x0=x0;
                find1=true;
            }
            res_y1=y1;
            res_x1=x1;

            draw.BresenhamLine(x0,y0,x1,y1, Color.RED);
            graphics.drawImage(img,0,0);
            return 0;
        }
        Clipping(x0,y0,(x0+x1)/2,(y0+y1)/2);
        Clipping((x0+x1)/2,(y0+y1)/2,x1,y1);
        return 0;
    }
}
