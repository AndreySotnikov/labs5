/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv_task1;

/**
 *
 * @author andrey
 */
public class MV_task1 {

    /**
     * @param args the command line arguments
     */
    public static double integral(int i) {
        if (i == 1) {
            System.out.println("J" + i + " : " + 1 / Math.E);
            return 1 / Math.E;
        } else {
            double aaa = integral(i - 1);
            double tmp = 1 - aaa * i;
            System.out.println("J" + i + " : " + tmp);
            return tmp;
        }
    }

    public static void mach_eps() {
        float eps = 1;
        int n = 0;
        while (1 + eps > 1) {
            eps /= 2;
            n++;
        }
        eps *= 2;
        n--;
        System.out.println("Eps float");
        System.out.println("Итерации: " + n + " Eps: " + eps);
        double eps1 = 1;
        n = 0;
        while (1 + eps1 > 1) {
            eps1 /= 2;
            n++;
        }
        eps1 *= 2;
        n--;
        System.out.println("Eps double");
        System.out.println("Итерации: " + n + " Eps: " + eps1);
    }

    public static void mach_eps_var() {
        float eps = 1;
        int n = 0;
        float s = 1 + eps;
        while (s > 1) {
            eps /= 2;
            n++;
            s = 1 + eps;
        }
        eps *= 2;
        n--;
        System.out.println("Eps с переменной s float");
        System.out.println("Итерации: " + n + " Eps: " + eps);

        double eps1 = 1;
        n = 0;
        double s1 = 1 + eps1;
        while (s1 > 1) {
            eps1 /= 2;
            n++;
            s1 = 1 + eps1;
        }
        eps1 *= 2;
        n--;
        System.out.println("Eps с переменной s double");
        System.out.println("Итерации: " + n + " Eps: " + eps1);
    }

    public static void main(String[] args) {
        System.out.println("Интеграл");
        integral(26);
        System.out.println("");
        mach_eps();
        mach_eps_var();
    }

}
