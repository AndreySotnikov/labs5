package sample;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

/**
 * Created by andrey on 07.10.14.
 */
public class Drawing {
    GraphicsContext graphics;
    PixelWriter writer;

    public Drawing(GraphicsContext graphics,PixelWriter writer) {
        this.graphics = graphics;
        this.writer=writer;
    }


    void BresenhamLine(int x0, int y0, int x1, int y1,Color color) {
        boolean steep = Math.abs(y1 - y0) > Math.abs(x1 - x0); // Проверяем рост отрезка по оси икс и по оси игрек
        // Отражаем линию по диагонали, если угол наклона слишком большой
        if (steep) {
            y0 = swap(x0, x0 = y0);
            y1 = swap(x1, x1 = y1);
            //Swap(ref x0, ref y0); // Перетасовка координат вынесена в отдельную функцию для красоты
            //Swap(ref x1, ref y1);
        }
        // Если линия растёт не слева направо, то меняем начало и конец отрезка местами
        if (x0 > x1) {
            x1 = swap(x0, x0 = x1);
            y1 = swap(y0, y0 = y1);
        }
        int dx = x1 - x0;
        int dy = Math.abs(y1 - y0);
        int error = dx / 2; // Здесь используется оптимизация с умножением на dx, чтобы избавиться от лишних дробей
        int ystep = (y0 < y1) ? 1 : -1; // Выбираем направление роста координаты y
        int y = y0;
        for (int x = x0; x <= x1; x++) {
            writer.setColor(steep ? y : x, steep ? x : y, color);

            //graphics.fillOval(steep ? y : x, steep ? x : y, 1, 1); // Не забываем вернуть координаты на место
            error -= dy;
            if (error < 0) {
                y += ystep;
                error += dx;
            }
        }
    }

    int swap(int a, int b) {  // usage: y = swap(x, x=y);
        return a;
    }

    void drawRect(int x0, int y0, int x1, int y1,Color color) {
        BresenhamLine(x0, y0, x1, y0,color);
        BresenhamLine(x0, y0, x0, y1,color);
        BresenhamLine(x1, y0, x1, y1,color);
        BresenhamLine(x0, y1, x1, y1,color);
    }
}
