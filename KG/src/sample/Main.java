package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.stage.Stage;

public class Main extends Application {
    public static int MAX_WIDTH=800;
    public static int MAX_HEIGHT=700;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Компьютерная графика");
        primaryStage.setScene(new Scene(root, MAX_WIDTH, MAX_HEIGHT,false, SceneAntialiasing.BALANCED));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
