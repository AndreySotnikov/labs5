package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.SceneAntialiasing;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import javax.annotation.Resources;
import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    Button btnDraw;
    @FXML
    Canvas cnInput;

    static int MAX_HEIGHT = 500;
    static int MAX_WIDTH = 800;

    WritableImage img = new WritableImage(MAX_WIDTH,MAX_HEIGHT);
    PixelWriter writer = img.getPixelWriter();
    GraphicsContext graphics;
    Drawing image;
    int x_start;
    int y_start;
    int last_x;
    int last_y;



    @FXML
    public void btnDrawClick() {
        graphics = cnInput.getGraphicsContext2D();
        image = new Drawing(graphics,writer);


        image.drawRect(200, 100,600, 400,Color.BLACK);
        Algo al = new Algo(graphics,img,200, 100, 600, 400,x_start,y_start,last_x,last_y);
        if (al.cut()==0){
        graphics.clearRect(0,0,MAX_WIDTH,MAX_HEIGHT);
        graphics.setStroke(Color.RED);
        graphics.strokeLine(al.res_x0,al.res_y0,al.res_x1,al.res_y1);
        graphics.setStroke(Color.BLACK);
        graphics.strokeLine(x_start,y_start,al.res_x0,al.res_y0);
        graphics.strokeLine(al.res_x1,al.res_y1,last_x,last_y);
        graphics.strokeRect(200,100,400,300);
        }else
            JOptionPane.showMessageDialog(null, "Отрезок за пределами прямоугольника");
    }

    @FXML
    public void canvasPressed(MouseEvent e){
        graphics = cnInput.getGraphicsContext2D();
        x_start=(int)e.getX();
        y_start=(int)e.getY();
    }
    @FXML
    public void CanvasDragged(MouseEvent e){
        graphics.clearRect(0,0,MAX_WIDTH,MAX_HEIGHT);
        img = new WritableImage(MAX_WIDTH,MAX_HEIGHT);
        writer = img.getPixelWriter();
        image = new Drawing(graphics,writer);
        image.BresenhamLine(x_start, y_start, (int) e.getX(), (int) e.getY(), Color.BLACK);
        image.drawRect(200, 100, 600, 400,Color.BLACK);
        last_x = (int)e.getX();
        last_y = (int)e.getY();
        graphics.drawImage(img,0,0);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        graphics = cnInput.getGraphicsContext2D();
        image = new Drawing(graphics,writer);
        graphics.drawImage(img,0,0);
        image.drawRect(200, 100,600, 400,Color.BLACK);
    }
}
